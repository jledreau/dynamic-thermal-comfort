"""
================================================================================
UPDATED FIALA DTS MODEL by Marika Vellei
================================================================================
"""

Tss=34.6
Tcs=37.3

# Get Dynamic Thermal Sensation (THIS IS FIALA original model)
def getDTS(R):
    """
    R is a dataframe including the time series of the skin and core temperatures and their gradients
    """

    Tskin_set = Tss
    Tcore_set = Tcs
    DTskin=(R['Tskin']-Tskin_set)
    DTcore=(R['Tcore']-Tcore_set)
    t=R['t']
    
    if (DTcore>-0.4)&(DTskin<4):
        g = 7.94 * np.exp(-0.902 / (DTcore + 0.4) + 7.612 / (DTskin - 4))
    else:
        g = 0
    
    if R['dTskin'] < 0: #SKIN COOLING
        dTskin_max = 0
        dTskin=R['dTskin']
    else: #SKIN WARMING
        dTskin_max = R['dTskin_max']
        dTskin=0
    
    if DTskin < 0:
        a = 0.301
    else:
        a = 1.078
    
    DTS=3*np.tanh(a*DTskin+g+(0.114*dTskin+0.137*np.exp(-0.681*t)*dTskin_max)/(1+g))
    
    return DTS

# Get New Dynamic Thermal Sensation (UPDATED VERSION with the dynamic part improved with additional data)
def getNewDTS(R):
    """
    R is a dataframe including the time series of the skin and core temperatures and their gradients
    """
    
    Tskin_set = Tss
    Tcore_set = Tcs
    
    DTskin=R['Tskin']-Tskin_set
    DTcore=R['Tcore']-Tcore_set
    
    dTskin=R['dTskin']
    
    if (DTcore>-0.4)&(DTskin<4):
        g = 7.94 * np.exp(-0.902 / (DTcore + 0.4) + 7.612 / (DTskin - 4))
    else:
        g = 0
    
    if DTskin < 0:
        a = 0.301
    else:
        a = 1.078
    
    if dTskin < 0:
        b = 0.3412
    else:
        b = 0.2755
    
    NewDTS=3*np.tanh(a*DTskin+g+(b*np.tanh(dTskin))/(1+g))
    
    return DTS
