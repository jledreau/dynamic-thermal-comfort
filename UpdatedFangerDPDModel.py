"""
================================================================================
UPDATED FANGER DPD MODEL by Marika Vellei
================================================================================
"""

# Get Static Thermal Sensation (from FIALA original model)
def getStaticPartDTS(x):
    """
    R is a dataframe including the time series of the skin and core temperatures and their gradients
    """
    Tskin_set = Tss
    Tcore_set = Tcs
    DTskin=x['Tskin']-Tskin_set
    DTcore=x['Tcore']-Tcore_set
    
    if (DTcore>-0.4)&(DTskin<4):
        g = 7.94 * np.exp(-0.902 / (DTcore + 0.4) + 7.612 / (DTskin - 4))
    else:
        g = 0
    
    if DTskin < 0:
        a = 0.301
    else:
        a = 1.078
    
    STS=3*np.tanh(a*DTskin+g)
    
    return STS

# Get Dynamic Percentage of Dissatisfied
def getDPD(x):
    """
    R is a dataframe including the time series of the skin temperatures gradient and the Dynamic Thermal Sensation
    """
    
    if x['STS'] > 0: # Warm Conditions
        if x['dTskin'] > 0: # WARMING GRADIENTS
            A = -0.0679554*np.tanh(x['dTskin']) 
            B = -0.54240851*np.tanh(x['dTskin'])
            
        else: # COOLING GRADIENTS
            A = -0.02509763*np.tanh(x['dTskin'])
            B = -0.21511396*np.tanh(x['dTskin'])
    else:
        if x['dTskin'] > 0: # WARMING GRADIENTS
            A = +0.02509763*np.tanh(x['dTskin'])
            B = -0.21511396*np.tanh(x['dTskin'])
            
        else: # COOLING GRADIENTS
            A = +0.0679554*np.tanh(x['dTskin']) 
            B = -0.54240851*np.tanh(x['dTskin'])
    
    DPD=(1-0.95*np.exp(-0.03353*np.power(x['DTS']+B, 4) -0.2179*np.power(x['DTS']+B, 2) + A))*100.
    
    return max(0, DPD)

