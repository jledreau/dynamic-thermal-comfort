R = pd.read_csv("no1_input.csv") 

df = R.reset_index(drop=True)

df['Time'] = pd.period_range('1/1/2011', freq='5min', periods=R['TA'].count())
df = df.set_index('Time').resample(rule='1min').interpolate(method='linear')

df['Tskin'] = np.nan
df['Tcore'] = np.nan
df = df.reset_index(drop=True)

GAGGE_2NODE_MODEL(df, 60.)

df['STS'] = df.apply(lambda x: getStaticPartDTS(x), axis=1)
df['DTS'] = df.apply(lambda x: getNewDTS(x), axis=1)
df['DPD'] = df.apply(lambda x: getDPD(x), axis=1)
