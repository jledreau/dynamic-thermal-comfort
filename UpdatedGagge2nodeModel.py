"""
================================================================================
UPDATED GAGGE 2-NODE MODEL by Marika Vellei
================================================================================
"""

def pv_sat(T):
    if T >= 0:
        pv_sat2 = pow(10, (2.7877 + (7.625*T)/(241 + T)))
    else :
        pv_sat2 = pow(10,(2.7877 + (9.756*T)/(272.7 + T)))
    return pv_sat2

def pv_calc(T, RH):
    pv_calc2 = (RH * pv_sat(T))/100
    return pv_calc2

def w(T, RH, p):
    w2 = 0.622 * (pv_calc(T, RH)/(p - pv_calc(T, RH)))
    return w2

def Cp_ah(T, RH, p):
    cpa = 1006
    cpv = 1830
    water = w(T, RH, p)
    Cp_ah2 = (cpa + water * cpv)/(1 + water)
    return Cp_ah2

def v_spe(T, RH, p):
    v_spe2 = (461.24 * (T + 273.15) * (0.622 + w(T, RH, p)))/p
    return v_spe2

# UPDATED GAGGE 2-NODE MODEL (also known as PIERCE 2-NODE MODEL)
def GAGGE_2NODE_MODEL(R, increment):
    """
    R is a dataframe including the time series of the personal and environmental input parameters:
    TA: air temperature in C, TR: mean radiant temperature in C, VEL: air velocity in m/s
    RH: relative humidity in %, clo: clothing insulation in clo, met: metabolic rate in met
    
    increment is the resolution of the time series in seconds
    """
    
    SBC = 5.67 * 0.00000001 ## stephan-Boltzmann constant
    AP = 101325. ## atmospheric pressure in Pa
    ATM = AP / 1000. * 0.00986923 ## conversion in atm
    
    ## CONSTANTES PHYSIOLOGIQUES HUMAINES
    KCLO = 0.25 ## coefficient augmentation surface d'echange
    Cst_sweat = 170. ## coefficient for sweating
    Cst_dilat = 200. ## coefficient for vasodilation between 50 and 240 ## 200. in (Fountain & Huizenga, 1995)  
    Cst_constr = 0.1 ## coefficient for vasoconstriction between 0.1 and 0.5
    masse = 70. ## body weight in kg ## 70. in (Fountain & Huizenga, 1995)  
    surface = 1.8 ## surface area of the nude body in m2 ## 1.2 in (Fountain & Huizenga, 1995)  
    Cp_body = 0.97*3600 ## thermal capacitance of body tissue in J/(kg*C)
    
    ## VALEURS DE CONSIGNES DE LA REGULATION DU CORPS HUMAIN
    T_skin_set = 34.4 ## neutral skin temperature in C between 33.7 and 34 ## 33.7 in (Fountain & Huizenga, 1995)
    T_core_set = 37. ## neutral core temperature in C between 36.6 and 36.8 ## 36.8 in (Fountain & Huizenga, 1995)
    T_body_set = 0.1*T_skin_set + 0.9*T_core_set ## mean body temperature in C
    SkinBloodFlow_set = 6.3 ## skin blood flow rate per unit skin surface area in L/(m2*h)
    
    ## INITIAL VALUES
    T_skin = 34.6
    T_core = 37.3
    SkinBloodFlow = SkinBloodFlow_set
    
    M_shiv = 0
    alpha = 0.1 ## fraction of total body mass attributed to the skin compartment
    E_skin = 0.1 * 1.1 * 58.2 
    
    dT = increment ## time-step of the transient simulation in seconds
    duree=(R['TA'].count()-1)*60. ## duration of the transient simulation in seconds
    temps=0
    while temps <= duree:
        
        TA=R.loc[temps/dT, 'TA'] ## air temperature in C
        TR=R.loc[temps/dT, 'TR'] ## mean radiant temperature in C
        VEL=R.loc[temps/dT, 'VEL'] ## air velocity in m/s
        RH=R.loc[temps/dT, 'RH']/100. ## relative humidity in %
        clo=R.loc[temps/dT, 'CLO'] ## clothing in clo
        met=R.loc[temps/dT, 'MET'] ## metabolic rate in met
        
        ## METABOLIC RATE in W/m2
        RM = met * 58.2 
        
        ## TOTAL METABOLIC RATE in W/m2
        Metab = RM + M_shiv
        
        v_walk = 0.0052 * (RM - 58.)
        corr_T = math.exp(0.042 - 0.398 * VEL + 0.066 * VEL**2 - 0.378 * v_walk + 0.094 * v_walk**2)
        if VEL > 3.5 or corr_T < 0.582:
            corr_T = 0.582
        
        clo = clo * corr_T
        VEL = math.sqrt(VEL**2 + v_walk**2)
        
        ## THERMAL RESISTANCE OF CLOTHING in (C*m2)/W
        R_clo = 0.155*clo
        
        ## dimensionless ratio needed to account for the increase in body surface area due to clothing
        if clo < 0.5:
            f_surf_clo = 1 + 0.2*clo
        else:
            f_surf_clo = 1 + 0.15*clo
        
        if clo <= 0:
            w_crit = 0.38 * pow(VEL, -0.29)
            I_clo = 1
        else:
            w_crit = 0.59 * pow(VEL, -0.08)
            I_clo = 0.45 * (4.9 - 6.5 * corr_T + 2.6 * corr_T**2)
        
        ## CONVECTIVE HEAT TRANSFER COEFFICIENT in W/(m2*C)
        h_c = 3. * pow(ATM, 0.53)
        if met < 0.85:
            h_c_free = 0
        else:
            h_c_free = 5.66 * pow((met-0.85)*ATM, 0.39) ## free convection
        h_c_forced = 8.6 * pow(VEL*ATM, 0.53) ## forced convection
        h_c = max(h_c, h_c_free, h_c_forced)
        
        ## RADIATIVE HEAT TRANSFER COEFFICIENT in W/(m2*C)
        h_r = 4.7 ## initial estimate
        
        ## DRY (CONVECTIVE + RADIATIVE) HEAT TRANSFER COEFFICIENT in W/(m2*C)
        h_g = h_r + h_c ## initial estimate
        
        ## DRY (CONVECTIVE + RADIATIVE) THERMAL RESISTANCE in (m2*C)/W
        R_air = 1/(f_surf_clo * h_g) ## initial estimate
        
        ## OPERATIVE TEMPERATURE in C
        T_op = (h_r * TR + h_c * TA)/h_g ## initial estimate
        
        ## CLOTHING TEMPERATURE in C
        T_clo = T_op + (T_skin - T_op)/(h_g * (R_air + R_clo)) ## initial estimate
        T_clo_OLD = T_clo + 0.5
        
        compteur = 0
        while abs(T_clo - T_clo_OLD) > 0.001:
            T_clo_OLD = T_clo
            h_r = 0.72 * SBC * ((TR + T_clo) + 2 * 273.15) * (pow((TR + 273.15), 2) + pow((T_clo + 273.15), 2)) * 0.97
            # h_r =  4. * 0.72 * SBC * pow(((TR + T_clo)/2 + 273.15), 3) ## in (Fountain & Huizenga, 1995)
            h_g = h_r + h_c
            R_air = 1/(f_surf_clo * h_g)
            T_op = (h_r * TR + h_c * TA)/h_g
            T_clo = (R_air * T_skin + R_clo * T_op)/(R_air + R_clo)
            compteur = compteur + 1
            if compteur > 20:
                break
        
        ## RATE OF CONVECTIVE AND RADIATIVE HEAT LOSS FROM THE BODY in W/m2
        DRY = (T_skin - T_op)/(R_air + R_clo)
        
        ## RATE OF ENERGY TRASPORT FROM CORE TO SKIN in W/m2
        Flx_core_skin = (T_core - T_skin) * (5.28 + 1.163 * SkinBloodFlow)
        
        T_expir = 32.5 + 0.066 * TA + 1.99 * 0.000001 * RH * pv_sat(TA)
        # T_expir = 34. ## in (Fountain & Huizenga, 1995)
        
        ## rate of RESPIRATORY heat loss due to convection in W/m2
        C_resp = 0.0014 * RM * (T_expir - TA)
        
        ## rate of RESPIRATORY heat loss due to evaporation in W/m2
        E_resp = 0.000017251 * RM * (pv_sat(35.5) - RH * pv_sat(TA))
        
        ## RATE OF ENERGY STORAGE IN THE CORE in W/m2
        ## heat balance equation for the core 
        SCR = Metab - Flx_core_skin - E_resp - C_resp
        
        ## RATE OF ENERGY STORAGE IN THE SKIN in W/m2
        ## heat balance equation for the skin compartment
        SSK = Flx_core_skin - DRY - E_skin
        
        ## modification des temperatures par l'effet de l'accumulation
        ## new temperatures are calculated in each iteration from the rates of heat storage in the core and skin
        dT_skin = SSK * surface * dT / (alpha * masse * Cp_body) ## in C/min
        dT_core = SCR * surface * dT / ((1-alpha) * masse *  Cp_body) ## in C/min
        
        T_skin = T_skin + dT_skin
        T_core = T_core + dT_core
        T_body = alpha * T_skin + (1 - alpha) * T_core
        
        R.loc[temps/dT, 'Tskin'] = T_skin
        R.loc[temps/dT, 'Tcore'] = T_core
        
        R.loc[temps/dT, 'dTskin'] = dT_skin*60. ## in C/h
        R.loc[temps/dT, 'dTcore'] = dT_core*60. ## in C/h
        
        #------------Modele de regulation du corps humain------------#
        
        ## SKIN
        signal_skin = T_skin - T_skin_set
        if signal_skin > 0:
            warm_skin = signal_skin
            cold_skin = 0
        else:
            warm_skin = 0
            cold_skin = -signal_skin
        
        ## CORE
        signal_core = T_core - T_core_set
        if signal_core > 0:
            warm_core = signal_core
            cold_core = 0
        else:
            warm_core = 0
            cold_core = -signal_core
        
        ## MEAN BODY
        signal_body = T_body - T_body_set
        if signal_body > 0 :
            warm_body = signal_body
            cold_body = 0
        else :
            warm_body = 0
            cold_body = -signal_body
        
        ## skin blood flow rate per unit skin surface area in L/(m2*h)
        
        # SkinBloodFlow = (SkinBloodFlow_set + Cst_dilat * warm_core)/(1 + Cst_constr * cold_skin) ## in (Fountain & Huizenga, 1995)
        
        dilat = 16. * (np.tanh(1.92 * (T_skin - T_skin_set) - 2.53) + 1.) * (T_skin - T_skin_set) + 30. * (np.tanh(3.51 * (T_core - T_core_set) - 1.48) + 1.) * (T_core - T_core_set)  # FIALA PhD thesis
        dilat = max(dilat, 0)
        constr = 35. * (np.tanh(0.29 * (T_skin - T_skin_set) + 1.11) - 1.) * (T_skin - T_skin_set) - 7.7 * (T_core - T_core_set) + 3. * min(0, T_skin - T_skin_set) * min(0, dT_skin * 60.)  # FIALA PhD thesis
        constr = max(constr, 0)
        SkinBloodFlow = ((25.01 + dilat) / (1 + constr * np.exp(-dilat/4.5))) * 0.9226 / surface
        
        if SkinBloodFlow > 378.:
            SkinBloodFlow = 378./surface
        if SkinBloodFlow < 0.5:
            SkinBloodFlow = 0.5
        
        alpha = 0.0417737 + 0.7451832/(SkinBloodFlow + 0.585417)
        
        ## regulatory sweating in g/(m2*h)
        
        # qm_sweat = Cst_sweat * warm_body * math.exp(warm_skin/10.7) ## in (Fountain & Huizenga, 1995)
        
        qm_sweat = ((0.65 * np.tanh(0.82 * (T_skin - T_skin_set) - 0.47) + 1.15) * (T_skin - T_skin_set) + (5.6 * np.tanh(3.14 * (T_core - T_core_set) - 1.83) + 6.4) * (T_core - T_core_set)) * 60. / surface  # FIALA PhD thesis
        qm_sweat = max(qm_sweat, 0)
        
        if qm_sweat*surface > 30.*60.:
            qm_sweat = 30.*60./surface
        
        ## RATE OF HEAT LOSS FROM REGULATORY SWEATING in W/m2
        E_sweat = 0.68 * qm_sweat
        
        ## calcul du nombre de Lewis
        Lewis = 2434 * v_spe(TA, RH*100, AP)/(Cp_ah(TA, RH*100, AP) * 1.04 * pow(0.83, (2/3))) * (18/8.32/(TA + 273.15))
        # Lewis = 2.02 * (T_skin+273.15) / 273.15 / 133.322 ## in (Fountain & Huizenga, 1995)
        # Lewis = 2.2 / ATM / 133.322 ## in (Fountain & Huizenga, 1995)
                
        ## evaporative heat transfer coefficient in W/m2*Pa
        h_e = (2.2 * h_c)/(1 + 0.928 * R_clo * h_c)/133.322
        
        ## evaporative resistance of air layer + evaporative resistance of clothing 
        R_vap_tot = 1/(f_surf_clo*h_e) + R_clo/(Lewis * I_clo)
        # R_vap_tot = R_air/Lewis + R_clo/(Lewis * I_clo) ## in (Fountain & Huizenga, 1995)
        
        ## MAXIMUM RATE OF HEAT LOSS FROM REGULATORY SWEATING in W/m2
        E_max = (pv_sat(T_skin) - RH * pv_sat(TA)) / R_vap_tot
        
        pcent_sweat = E_sweat/E_max # part d'energie echangee due a la sudation
        pcent_wet = 0.06 + 0.94 * pcent_sweat # part de la surface du corps mouille
        
        ## RATE OF HEAT LOSS FROM DIFFUSION OF WATER VAPOUR THROUGH THE SKIN in W/m2
        E_diff = pcent_wet * E_max - E_sweat
        
        ## RATE OF HEAT LOSS FROM SWEAT EVAPORATION in W/m2
        E_skin = E_sweat + E_diff
        
        ## Sudation supercritique
        if pcent_wet > w_crit :
            pcent_wet = w_crit
            pcent_sweat = (w_crit-0.06)/0.94
            E_sweat = pcent_sweat * E_max
            E_diff = 0.06 * (1 - pcent_sweat) * E_max
            E_skin = E_sweat + E_diff
        # Condensation (la pression de vapeur saturante a la temperature de la peau est inferieure a la pression de vapeur de l'air ambiant)
        elif E_max < 0 :
            E_diff = 0
            E_sweat = 0
            pcent_wet = w_crit
            pcent_sweat = w_crit
            E_skin = E_max
        
        ##RATE OF ENERGY RELEASED BY SHIVERING in W/m2
        
        # M_shiv = 19.4 * cold_skin * cold_core ## in (Fountain & Huizenga, 1995)
        
        if qm_sweat > 0:
            M_shiv = 0
        else:
            M_shiv = (10. * (np.tanh(0.51 * (T_skin - T_skin_set) + 4.19) - 1) * (T_skin - T_skin_set) - 27.5 * (T_core - T_core_set) - 28.2 + 1.9 * min(0, T_skin - T_skin_set) * min(0, dT_skin * 60.)) / surface  # FIALA PhD thesis
        M_shiv = max(M_shiv, 0)
        
        if M_shiv * surface > 350.:
            M_shiv = 350. / surface
        
        temps = temps + dT