**Welcome to the Dynamic Thermal Comfort Tool !**

To help researchers to evaluate UNIFORM but not stationary (thus TRANSIENT) thermal conditions,
we provide a new tool, which can be used to predict the whole-body dynamic thermal sensation and thermal comfort.

The tool comprises:
* a thermo-physiological model able to predict the body core and mean skin temperatures under UNIFORM and TRANSIENT environmental conditions
* a dynamic thermal perception model, which uses the simulated temperatures to predict thermal sensation and thermal comfort.

The selected thermo-physiological model is an updated version of the classical **Gagge’s 2-node model**.
For predicting the thermal sensation vote, we use an updated version of **Fiala’s Dynamic Thermal Sensation (DTS) model**.
Finally, for modelling the last step of thermal perception, i.e. thermal comfort, we provide a new dynamic version of the well-known
**Fanger’s Predicted Percentage of Dissatisfied (PPD) index**. 

![](image/Figure0.png)

As an example, in the figure below the model is used to predict occupants' dynamic thermal comfort conditions
under a varying level of metabolic rate with a constant air temperature at 21°C

**Preview**

![](image/Figure.png)

The Dynamic Thermal Comfort model has been coupled with **an Agent-Based Stochastic Model of Thermostat Adjustments** and used to derive **a simplified time/temperature relationship for modelling overriding rates**. This relationship is shown in the figure below. The data are stored in the cvs file.

![](image/ORII.png)

**Release history**
*  v0 : 14/04/2020


**License**

Licensed by La Rochelle University/LaSIE under a BSD 3 license (https://opensource.org/licenses/BSD-3-Clause).


**References**

Marika Vellei & Jérôme Le Dréau, On the prediction of dynamic thermal comfort under uniform environments, Proceedings of the 2020 Windsor Conference: Resilient Comfort, Windsor, UK, 2020.

Vellei, M., Martinez, S., Le Dréau, J., Agent-based stochastic model of thermostat adjustments: A demand response application, Energy and Buildings 238: 110-846 (2021).

